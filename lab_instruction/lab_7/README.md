# Lab 7 : Pengenalan _Web Service_

CSGE602022 - Web Design & Programming (Perancangan & Pemrograman Web) @
Faculty of Computer Science Universitas Indonesia, Odd Semester 2017/2018

* * *

## Tujuan Pembelajaran

Setelah menyelesaikan tutorial ini, mahasiswa diharapkan untuk mengerti:
- Apa itu Webservice, ajax dan json
- Mampu menggunakan webservice, ajax, dan json

## Hasil Akhir Lab
- Membuat halaman berisi daftar mahasiswa fasilkom ui menggunakan api dari api-dev.cs.ui.ac.id

## Self-Reflection
Sebelum mempelajari tutorial ini, mari kita mereview beberapa pengetahuan dasar yang sudah pernah kita bahas dan pelajari sebelumnya.

- Apakah kamu tahu cara web bekerja? Apa yang dilakukan browsermu saat membuka suatu link? Apa itu HTTP? Requests dan response? Jika tidak, maka kamu perlu baca dan pelajari kembali!
- Apa itu Django? Model, Views, Template?
- HTML, CSS, dan DOM? Apa kegunaan javascript? Dan bagaimana cara menjalankan suatu script javascript pada suatu page?

## Pengenalan (PENTING UNTUK DIPAHAMI)

#### API
- API merupakan suatu interface yang dalam hal ini berupa sekumpulan fungsi/method yang digunakan suatu program untuk berkomunikasi/menjalankan/menggunakan program lain tanpa perlu mengetahui kompleksitas dari fungsi tersebut.
- Sebagai contoh, Java menyediakan banyak sekali api (lihat https://docs.oracle.com/javase/7/docs/api/)
- Sebagai contohnya ketika kita ingin membuat GUI pada program java kita, kita dapat menggunakan swing api dengan melakukan import dan menggunakan fungsi-fungsi yang disediakan, kemudian kita dapat menampilkan sebuah panel yang terdapat text field atau button di dalamnya dengan menggunakan fungsi-fungsi tersebut tanpa mengetahui kompleksitas di dalamnya.

#### Apa itu Web Service
- Web service merupakan API yang dibungkus dalam HTTP yang membuat dua mesin berbeda dapat berkomunikasi.

#### Apa itu Json
- JSON (JavaScript Object Notation) adalah format pertukaran data yang ringan, mudah dibaca dan ditulis oleh manusia, serta mudah diterjemahkan dan dibuat (generate) oleh komputer.
- JSON merupakan format teks yang tidak bergantung pada bahasa pemprograman apapun karena menggunakan gaya bahasa yang umum digunakan oleh programmer keluarga C termasuk C, C++, C#, Java, JavaScript, Perl, Python dll, oleh karena sifat-sifat tersebut, menjadikan JSON ideal sebagai bahasa pertukaran-data.

Berikut ini merupakan contoh format json

```
{
    “Nama”: “kak pewe”,
    “Age”: 2017,
    “Message”: [“semoga soalnya susah”, ”yang buat aufa”] //list message
}
```

- untuk lebih lengkapnya bisa lihat di http://www.json.org/json-id.html

#### Apa itu AJAX
- Asynchronous JavaScript and XMLHTTP, atau disingkat AJaX, adalah suatu teknik pemrograman berbasis web untuk menciptakan aplikasi web interaktif.
- Tujuannya adalah untuk melakukan pertukaran data dengan server di belakang layar, sehingga halaman web tidak harus dibaca ulang(reload) secara keseluruhan setiap kali seorang pengguna melakukan perubahan.
- Contoh: Jika anda membuka https://www.tokopedia.com/search?st=product&q=microwave+oven&sc=984 lalu coba tambahkan filter pada pencarian, maka pilihan barang belanjaan akan berubah tanpa me-reload page

Secara umum code ajax dalam jquery sebagai berikut
```javascript
$.ajax({<name>:<value>, <name>:<value>, ... })
```
Yang mana “name” atau “value” dapat diisi dengan berbagai nilai contoh:
```javascript
<script>
$(document).ready(function(){
    $("#buttonPanggilAjax").click(function(){
        $.ajax({
           url: "example.com", // url yang akan dipanggil
           Data: { data1:”contoh1” , data2:”contoh2 } // data yang akan dikirim ke example.com
           dataType: “json” // atau apapun tipe data yang diharapkan didapat dari example.com
           success: function(result){
                 // disini akan berisi code yang akan dieksekusi jika example.com berhasil dipanggil
                 // result merupakan data yang diperoleh dari example.com
            },
           Error: function(xhr, status, error) {
                // disini akan berisi code yang akan dieksekusi jika example.com gagal dipanggil
            }
       });
    });
});
</script>
```

## Cara menggunakan API (CUKUP DIBACA SAJA, TIDAK USAH DIKERJAKAN)
1. Pada tutorial lab kali ini, api yang akan kita gunakan untuk latihan adalah api milik fasilkom, yaitu `https://api-dev.cs.ui.ac.id/`. Kalian bisa membuka link tersebut untuk mengetahui cara penggunaannya dan dokumentasi dari api-dev.cs.ui.ac.id itu sendiri.
2. Untuk menggunakan api dari api.cs.ui.ac.id, hal yang kita perlukan adalah:
  A) Access token
  B) Client id
Pada lab ini, Kak Pewe sudah membuatkan helper untuk memudahkan kalian mendapatkan access token maupun client id. Silahkan pelajari script python yang ada pada langkah pengerjaan lab di bawah.
3. Pada umumnya, terdapat beberapa method yang sering digunakan dalam menggunakan API, yaitu GET, POST, PUT, dan DELETE.
    * `GET` digunakan untuk mengambil data dari API kalian
    * `POST` digunakan untuk membuat data baru di API
    * `PUT` digunakan untuk mengubah data yang sudah ada di API
    * `DELETE` digunakan untuk menghapus data dari API
4. Contoh penggunaan API api-dev.cs.ui.ac.id adalah sebagai berikut:
    ```
    https://api-dev.cs.ui.ac.id/siakngcs/mahasiswa-list/?access_token=SECRET_TOKEN&client_id=SECRET_CLIENT_ID
    ```
5. Berikut adalah contoh response yang diberikan oleh API api-dev.cs.ui.ac.id
    ```
    {
        "count": 6800,
        "next": "https://api-dev.cs.ui.ac.id/siakngcs/mahasiswa-list/?access_token=SECRET_TOKEN&client_id=SECRET_CLIENT_ID&page=2",
        "previous": null,
        "results": [
            ...

            {
                "url": "https://api-dev.cs.ui.ac.id/siakngcs/mahasiswa/1908989055/",
                "npm": "1908989055",
                "nama": "Kak Pewe",
                "alamat_mhs": "Lab.1103 dan Lab.1107",
                "kd_pos_mhs": "99999",
                "kota_lahir": "Depok",
                "tgl_lahir": "2018-12-18",
                "program": [
                    {
                        "url": "https://api-dev.cs.ui.ac.id/siakngcs/program/96554/",
                        "periode": {
                            "url": "https://api-dev.cs.ui.ac.id/siakngcs/periode/35/",
                            "term": 1,
                            "tahun": 2017
                        },
                        "kd_org": "01.00.12.01",
                        "nm_org": "Ilmu Komputer",
                        "nm_prg": "S1 Reguler",
                        "angkatan": 2015,
                        "nm_status": "Aktif",
                        "kd_status": "1"
                        },

                        ...
                    }
                ]
            },
        ...
    }

    ```

## Membuat Halaman Daftar Mahasiswa Fasilkom (MULAI KERJAKAN LAB DARI SINI)
1. Jangan lupa jalankan virtual environment kalian
2. Buatlah _apps_ baru bernama `lab_7`
3. Masukkan `lab_7` kedalam `INSTALLED_APPS`
4. Buatlah _Test_ baru kedalam `lab_7/tests.py`
5. _Commit_ lalu _Push_ pekerjaan kalian, maka kalian akan melihat _UnitTest_ kalian akan _error_
6. Buatlah konfigurasi URL di `praktikum/urls.py` untuk `lab_7`
7. Buatlah konfigurasi URL di `lab_7/urls.py`
    ```python
        from django.conf.urls import url
        from .views import *

        urlpatterns = [
             url(r'^$', index, name='index'),
             url(r'^add-friend/$', add_friend, name='add-friend'),
             url(r'^validate-npm/$', validate_npm, name='validate-npm'),
             url(r'^delete-friend/(?P<friend_id>[0-9]+)/$', delete_friend, name='delete-friend'),
             url(r'^get-friend-list/$', friend_list_json, name='get-friend-list')
        ]
    ```
8. Buat sebuah package bernama `api_csui_helper` lalu buat file `csui_helper.py` ke folder package tersebut
    ```python
    import requests
    import os
    import environ

    root = environ.Path(__file__) - 3 # three folder back (/a/b/c/ - 3 = /)
    env = environ.Env(DEBUG=(bool, False),)
    environ.Env.read_env('.env')
    API_MAHASISWA_LIST_URL = "https://api.cs.ui.ac.id/siakngcs/mahasiswa-list/"


    class CSUIhelper:
        class __CSUIhelper:
            def __init__(self):
                self.username = env("SSO_USERNAME")
                self.password = env("SSO_PASSWORD")
                self.client_id = 'X3zNkFmepkdA47ASNMDZRX3Z9gqSU1Lwywu5WepG'
                self.access_token = self.get_access_token()

            def get_access_token(self):
                try:
                    url = "https://akun.cs.ui.ac.id/oauth/token/"

                    payload = "username=" + self.username + "&password=" + self.password + "&grant_type=password"
                    headers = {
                        'authorization': "Basic WDN6TmtGbWVwa2RBNDdBU05NRFpSWDNaOWdxU1UxTHd5d3U1V2VwRzpCRVFXQW43RDl6a2k3NEZ0bkNpWVhIRk50Ymg3eXlNWmFuNnlvMU1uaUdSVWNGWnhkQnBobUU5TUxuVHZiTTEzM1dsUnBwTHJoTXBkYktqTjBxcU9OaHlTNGl2Z0doczB0OVhlQ3M0Ym1JeUJLMldwbnZYTXE4VU5yTEFEMDNZeA==",
                        'cache-control': "no-cache",
                        'content-type': "application/x-www-form-urlencoded"
                    }

                    response = requests.request("POST", url, data=payload, headers=headers)

                    return response.json()["access_token"]
                except Exception:
                    raise Exception("username atau password sso salah, input : [{}, {}] {}".format(self.username, self.password, os.environ.items()))

            def get_client_id(self):
                return self.client_id

            def get_auth_param_dict(self):
                dict = {}
                acces_token = self.get_access_token()
                client_id = self.get_client_id()
                dict['access_token'] = acces_token
                dict['client_id'] = client_id

                return dict

            def get_mahasiswa_list(self):
                response = requests.get(API_MAHASISWA_LIST_URL,
                                        params={"access_token": self.access_token, "client_id": self.client_id})
                mahasiswa_list = response.json()["results"]
                return mahasiswa_list

        instance = None

        def __init__(self):
            if not CSUIhelper.instance:
                CSUIhelper.instance = CSUIhelper.__CSUIhelper()
    ```
9. Jika kita lihat, pada file `csui_helper.py` terdapat kode seperti
    ```python
    env('SSO_USERNAME')
    env('SSO_PASSWORD')
    ```
    Kode tersebut mengambil variable dari sistem tempat kode tersebut dijalankan. Artinya kita perlu menset variable yang digunakan tersebut di sistem milik kita.
    Silahkan buka heroku > pilih aplikasi kalian > masuk ke menu setting
    Buka bagian Reveal Config Vars, dan isi sesuai data diri kalian. Ada dua hal yang perlu kalian set disini

    * Isi KEY dengan SSO_USERNAME dan isi value dengan username siak kalian
    * Isi KEY dengan SSO_PASSWORD dan isi value dengan password siak kalian

   Untuk keperluan localhost, silahkan kalian buat file .env (satu directory dengan file manage.py) tambahkan variable SSO_USERNAME dan SSO_PASSWORD
   contoh file .env
   ```python
   SSO_USERNAME=anang
   SSO_PASSWORD=hermansyah
   ```
   Penggunaan variable dari env ditujukan untuk menjaga kerahasiaan username dan password kalian agar tidak diketahui oleh orang lain.

   __PENTING => Masukkan file .env ke dalam file .gitignore agar file kalian yang berisi password tersebut tidak ter-push ke gitlab kalian__

10. masukan kode berikut pada views.py
    ```python
    from django.shortcuts import render
    from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
    from django.views.decorators.csrf import csrf_exempt
    from .models import Friend
    from .api_csui_helper.csui_helper import CSUIhelper
    from django.core import serializers
    import os
    import json

    response = {}
    csui_helper = CSUIhelper()


    def index(request):
        mahasiswa_list = csui_helper.instance.get_mahasiswa_list()

        friend_list = Friend.objects.all()
        html = 'lab_7/lab_7.html'
        response = {"mahasiswa_list": mahasiswa_list, "friend_list": friend_list}
        return render(request, html, response)


    def friend_list(request):
        friend_list = Friend.objects.all()
        response['friend_list'] = friend_list
        html = 'lab_7/daftar_teman.html'
        return render(request, html, response)

    def friend_list_json(request): # update
        friends = [obj.as_dict() for obj in Friend.objects.all()]
        return JsonResponse({"results": friends}, content_type='application/json')

    @csrf_exempt
    def add_friend(request):
        if request.method == 'POST':
            npm = request.POST['npm']
            friend = Friend(friend_name=name, npm=npm)
            friend.save()
            return HttpResponse("berhasil")


    def delete_friend(request, friend_id):
        Friend.objects.filter(id=friend_id).delete()
        return HttpResponseRedirect('/lab-7/')


    @csrf_exempt
    def validate_npm(request):
        npm = request.POST.get('npm', None)
        data = {
            'is_taken': Friend.objects.filter(npm=npm).exists()
        }
        return JsonResponse(data)

    def model_to_dict(obj):
        data = serializers.serialize('json', [obj,])
        struct = json.loads(data)
        data = json.dumps(struct[0]["fields"])
        return data

    ```

    __Pada line 9, terdapat code `csui_helper = CSUIhelper()`, menurut kalian apa yang terjadi pada line ini?__

11. Buatlah _Models_ untuk `Friend` di dalam `lab_7/models.py`
    ```python
        # -*- coding: utf-8 -*-
        from __future__ import unicode_literals

        from django.db import models


        # Create your models here.
        class Friend(models.Model):
            friend_name = models.CharField(max_length=400)
            npm = models.CharField(max_length=250, unique=True)
            added_at = models.DateField(auto_now_add=True)

            def as_dict(self):
                return {
                    "friend_name": self.friend_name,
                    "npm": self.npm,
                }

    ```
12. Jalankan perintah `makemigrations` dan `migrate`

13. Buatlah `lab_7.css` di dalam `lab_7/static/css`
   ```css
   body{
       margin-top: 70px;
   }
   /* Custom navbar style */
   .navbar-static-top {
       margin-bottom: 19px;
   }
   .navbar-default .navbar-nav>li>a {
       cursor: pointer;
   }
   /* Textarea not resizeable */
   textarea {
       resize:none
   }
   ```

14. Buatlah base.html di dalam `lab_7/templates/lab_7/layout`
   ```html
   {% load staticfiles %}
   {% load static %}
   <!DOCTYPE html>
   <html lang="en">
   <head>
       <meta charset="utf-8">
       <meta name="viewport" content="width=device-width, initial-scale=1">
       <meta http-equiv="X-UA-Compatible" content="IE=edge">
       <meta name="description" content="LAB 5">
       <meta name="author" content="{{author}}">
       <!-- bootstrap csss -->
       <link href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css" rel="stylesheet">
       <link rel="stylesheet" type="text/css" href="{% static 'css/lab_7.css' %}" />
       <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Droid+Sans:400,700">

       <title>
           {% block title %} Lab 7 {% endblock %}
       </title>
   </head>
   <body>
       <header>
           {% include "lab_7/partials/header.html" %}
       </header>
       <content>
               {% block content %}
                   <!-- content goes here -->
               {% endblock %}
       </content>
       <footer>
           <!-- TODO Block Footer dan include footer.html -->
           {% block footer %}
           {% include "lab_7/partials/footer.html" %}
           {% endblock %}
       </footer>
       <!-- Jquery n Bootstrap Script -->
       <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
       <script type="application/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
       {% block javascript %}
       {% endblock %}
   </body>
   </html>
   ```
15. Buatlah `lab_7.html` di dalam `lab_7/templates/lab_7/`
   ```html
    {% extends "lab_7/layout/base.html" %}
    {% block content %}
    <section name="mahasiswa-list" id="mahasiswa-list">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-lg-8">
                    <h2> Mahasiswa Fasilkom</h2>
                    <div class="list-group">
                        {% if mahasiswa_list %}
                            {% for mahasiswa in mahasiswa_list %}
                                <a class="list-group-item clearfix">
                                    {{ mahasiswa.nama }} ({{ mahasiswa.npm }})
                                    <span class="pull-right">
                            <span class="btn btn-xs btn-default" onClick="addFriend('{{ mahasiswa.nama }}', '{{ mahasiswa.npm }}')">
                                Tambah sebagai teman
                            </span>
                        </span>
                                </a>
                            {% endfor %}
                        {% else %}
                            <div class="alert alert-danger text-center">
                                <strong>Opps!</strong> Tidak ada mahasiswa
                            </div>
                        {% endif %}
                    </div>
                </div>
                <div class="col-lg-4">
                    <h2> Teman Saya </h2>
                    <div class="list-group" id="friend-list">
                        {% if friend_list %}
                            {% for friend in friend_list %}
                                <a class="list-group-item clearfix">
                                    {{ friend.friend_name }} ({{ friend.npm }})
                                </a>
                            {% endfor %}
                        {% else %}
                            <div class="alert alert-danger text-center">
                                <strong>Opps!</strong> Tidak ada teman
                            </div>
                        {% endif %}
                    </div>
                    <form id="add-friend" action="#">
                        {% csrf_token %}
                        <label for="field_npm">npm</label>
                        <input id="field_npm" type="text" name="npm" class="form-control"/>
                        <label for="field_name">name</label>
                        <input id="field_name" type="text" name="name" class="form-control"/>
                        <button type="submit">Tambah</button>
                    </form>
                </div>
            </div>
        </div>
    </section>
    {% endblock %}
    ```
16. TAMBAHKAN code javascript berikut pada `lab_7.html` di dalam `lab_7/templates/lab_7/`
    ```html
    ...
    {% block javascript %}
    <script>
        var addFriend = function(nama, npm) {
            $.ajax({
                method: "POST",
                url: "{% url "lab-7:add-friend" %}",
                data: { name: nama, npm: npm},
                success : function (response) {
                    html = '<a class="list-group-item clearfix">' +
                        nama+ ' (' + npm + ')' +
                        '</a>';
                    $("#friend-list").append(html)
                },
                error : function (error) {
                    alert(error.responseText)
                }
            });
        };

        $("#add-friend").on("submit", function () {
           name = $("#field_name").val()
           npm = $("#field_npm").val()
            addFriend(name, npm)
            event.preventDefault();
        });

        $("#field_npm").change(function () {
            console.log( $(this).val() );
            npm = $(this).val();
            $.ajax({
                method: "POST",
                url: '{% url "lab-7:validate-npm" %}',
                data: {
                    'npm': npm
                },
                dataType: 'json',
                success: function (data) {
                    console.log(data)
                    if (data.is_taken) {
                        alert("Mahasiswa dengan npm seperti ini sudah ada");
                    }
                }
            });
        });
    </script>
    {% endblock %}
    ```
17. PELAJARI apa yang dilakukan kode javascript tersebut
18. jalankan webserver kalian, jika ada sedikit error, coba analisa dan solve error yang terjadi


#### Implementasi Ajax
19. buatlah sebuah halaman yang menampilkan daftar teman

20. gunakan file pendukung 'daftar-teman.html'
    ```html
    {% extends "lab_7/layout/base.html" %}

    {% block content %}
        <section name="friend-list" id="friend-list">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-lg-8">
                        <h2> friend Fasilkom</h2>
                        <div id="friend-list" class="list-group">

                        </div>
                    </div>
                </div>
            </div>
        </section>
    {% endblock %}
    {% block javascript %}
        <script>
            $( document ).ready(function () {
                {# lengkapi pemanggilan ajax berikut untuk mengambil daftar teman yang ada di database #}
                $.ajax({
                    method: "GET",
                    url: "{% url 'lab-7:get-friend-list' %}", // update
                    success: function (response) {
                        #tampilkan list teman ke halaman
                        #hint : gunakan fungsi jquery append()
                    },
                    error: function(error){
                        #tampilkan pesan error
                    }
                });
            });
        </script>
    {% endblock %}
    ```
21. implementasikan pemanggilan ajax tersebut

## Checklist

### Mandatory(PENTING DIPERHATIKAN BAIK CHECKLIST MAUPUN PERTANYAAN)
1. Membuat halaman untuk menampilkan semua mahasiswa fasilkom
    1. [ ] Terdapat list yang berisi daftar mahasiswa fasilkom. 
    __Bagaimana mekanisme daftar mahasiswa fasilkom bisa muncul pada browser?__
    2. [ ] Buatlah tombol untuk dapat menambahkan list mahasiswa kedalam daftar teman (implementasikan menggunakan ajax) dan tampilkan alert berhasil menambahkan teman jika berhasil.
    __Bagaimana proses penambahan teman sejak tombol tambahkan teman di klik?__
    3. [ ] Mengimplentasikan validate_npm untuk mengecek apakah teman yang ingin dimasukkan sudah ada didalam daftar teman atau belum.
    __Jelaskan ide dari proses validasi NPM ini!__
    4. [ ] Membuat pagination (hint: salah satu data yang didapat dari kembalian api.cs.ui.ac.id adalah `next` dan `previous` yang bisa digunakan dalam membuat pagination)
       Documentation Django Pagination: https://docs.djangoproject.com/en/2.0/topics/pagination/
       Example of django pagination: https://simpleisbetterthancomplex.com/tutorial/2016/08/03/how-to-paginate-with-django.html
    __Apa tujuan dari digunakannya pagination?__
2. Membuat halaman untuk menampilkan daftar teman
    1. [ ] Terdapat list yang berisi daftar teman, data daftar teman didapat menggunakan ajax.
    __Jelaskan proses menampilkan daftar teman!__
    2. [ ] Buatlah tombol untuk dapat menghapus teman dari daftar teman (implementasikan menggunakan ajax).
    __Jelaskan proses menghapus teman dari friend list!__
3. Pastikan kalian memiliki _Code Coverage_ yang baik
    1. [ ] Jika kalian belum melakukan konfigurasi untuk menampilkan _Code Coverage_ di Gitlab maka lihat langkah `Show Code Coverage in Gitlab` di [README.md](https://gitlab.com/PPW-2017/ppw-lab/blob/master/README.md)
    2. [ ] Pastikan _Code Coverage_ kalian 100% (Buat test case yang baik)


### Additional
1. Membuat halaman yang menampilkan data lengkap teman
    1. [ ] Halaman dibuka setiap kali user mengklik salah satu teman pada halaman yang menampilkan daftar teman
    2. [ ] Tambahkan google maps yang menampilkan alamat teman pada halaman informasi detail (hint: https://developers.google.com/maps/documentation/javascript/)
