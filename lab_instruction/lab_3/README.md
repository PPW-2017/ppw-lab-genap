# Lab 3: Pengenalan _models_ dan TDD

CSGE602022 - Web Design & Programming (Perancangan & Pemrograman Web) @
Faculty of Computer Science Universitas Indonesia, Odd Semester 2017/2018

* * *

## Tujuan Pembelajaran

Setelah menyelesaikan tutorial ini, mahasiswa diharapkan untuk mengerti :

- Mengerti penggunaan _models_ pada _Django Project_
- Disiplin TDD dalam pengembangan _Web Application_

## Membuat Halaman _Diary_ (Menggunakan Disiplin TDD)

1. Jalankan _Virtual Environment_ kalian

2. Buatlah sebuah apps baru bernama `lab_3`

3. Buat berkas HTML `to_do_list_error.html` di _folder_ `templates` yang berada dalam `lab_3` yang berisi
    ```python
    {% extends "base.html" %}
    {% block content %}
    <h3 style="text-align: center">Hai semua, berikut adalah Catatan Harianku</h3>
    <br><br>
    <div class="row">
        <div class="col-md-6 col-xs-6">
            <table align="center" style="text-align: center">
                <tr>
                    <th>Tanggal</th>
                    <th>Yang akan dikerjakan</th>
                </tr>
                {% for diary_data in diary_dict %}
                    <tr>
                        <td class="date">{{ diary_data.date }}</td>
                        <td class="activity">{{ diary_data.activity }}</td>
                    </tr>
                {% endfor %}
            </table>
        </div>
        <div class="col-md-6 col-xs-6">
            <form method="POST" action="add_activity/">
                {% csrf_token %}
                <table>
                    <tr>
                        <td>
                            Tanggal
                        </td>
                        <td>
                            <input type="datetime-local" name="date" id="date" required="required"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Kegiatan
                        </td>
                        <td>
                            <input type="text" name="activity" id="activity" size="25"/>
                        </td>
                    </tr>
                </table>
                <button type="submit" class="button btn-success">Submit</button>
            </form>
        </div>
    </div>
    {% endblock %}
    ```
4. Masukkan `lab_3` ke dalam INSTALLED_APPS di dalam `praktikum/settings.py`

5. Masukkan _Test Case_ Berikut ke dalam `lab_3/tests.py`
    ```python
        from django.test import TestCase, Client
        from django.urls import resolve
        from .views import index

        class Lab3Test(TestCase):
            def test_lab_3_url_is_exist(self):
                response = Client().get('/lab-3/')
                self.assertEqual(response.status_code,200)

            def test_lab_3_using_to_do_list_template(self):
                response = Client().get('/lab-3/')
                self.assertTemplateUsed(response, 'to_do_list.html')

            def test_lab_3_using_index_func(self):
                found = resolve('/lab-3/')
                self.assertEqual(found.func, index)
    ```
    Berikut adalah _Test Case_ yang akan memastikan bahwa URL `<YOURHOSTNAME>/lab-3/` **bisa diakses, menggunakan fungsi index di dalam `views.py`, dan
    menggunakan template yang bernama `to_do_list.html`**

6. Jika kalian menjalankan _test_ secara lokal, maka kalian bisa melihat terjadi _error_ (analisalah kenapa error bisa terjadi). Gunakan perintah

    > python manage.py test

7. Untuk men-_solve_ semua _Test Case_, maka pertama - tama kalian harus membuat **konfigurasi URL**. Berikan
konfigurasi URL untuk `lab_3` (Buat berkas `urls.py` di dalam `lab_3`, lalu masukkan kode berikut) :
    ```python
        from django.conf.urls import url
        from .views import index
        #url for app
        urlpatterns = [
            url(r'^$', index, name='index'),
        ]
    ```

8. Masukkan kode berikut kedalam `lab_3/views.py` untuk dapat menampilkan berkas `to_do_list.html`:
    ```python
        from django.shortcuts import render
        # Create your views here.
        diary_dict = {}
        def index(request):
            return render(request, 'to_do_list.html', {'diary_dict' : diary_dict})
    ```

9. Sisipkan kode berikut kedalam konfigurasi URL untuk `lab_3` ke dalam `praktikum/urls.py`:
    ```python
        ...........
        import lab_3.urls as lab_3
        ...........

        urlpatterns = [
            .............
            url(r'^lab-3/', include(lab_3,namespace='lab-3')),
        ]
    ```
    `.....` menandakan kode kalian yang sudah ada, sehingga kode yang tertulis disini cukup kalian sisipkan bukan untuk di _copy-paste_

10. Silahkan jalankan _test_ kalian lagi maka kalian bisa melihat semua _Test Case_ akan ter-_solved_ (jika belum, baca baik-baik line code `test.py` yang tidak passed lalu lakukan analisa)

    > `python manage.py test`

11. Coba jalankan web kalian secara lokal:

    >python manage.py runserver 8000

    buka halaman lab-3 kalian, kalian bisa lihat bahwa sudah ada tabel kosong dan form untuk menambahkan kegiatan kalian, coba masukkan tanggal dan kegiatan lalu tekan tombol submit. Kalian akan menjumpai error, itu terjadi tentu karena kalian baru membuat tampilannya, sementara _backend_ untuk fitur tesebut belum kalian buat

12. Untuk membuat fitur yang akan menambahkan dan menampilkan _list_ aktifitas maka pertama - tama kalian harus membuat
**models** terlebih dahulu. Sisipkan _Test Case_ berikut kedalam `lab_3/tests.py` :
    ```python
        .........
        from .models import Diary
        from django.utils import timezone

        class Lab3Test(TestCase):
            ......................
            def test_model_can_create_new_activity(self):
                #Creating a new activity
                new_activity = Diary.objects.create(date=timezone.now(),activity='Aku mau latihan ngoding deh')

                #Retrieving all available activity
                counting_all_available_activity = Diary.objects.all().count()
                self.assertEqual(counting_all_available_activity,1)
    ```
13. Jalankan _Test Case_ kalian, maka akan kembali muncul _Error_ pada _Test Case_

    > Trivia
    >
    > Inilah langkah - langkah TDD yang nantinya akan kalian lakukan. Untuk membuat fitur, kalian membuat _Test Case_
    terlebih dahulu, dan ketika dijalankan _Test Case_ tersebut harus _Error_ (RED). Selanjutnya kalian harus membuat
    suatu fungsi yang menyelesaikan _Test Case_ tersebut (GREEN)

14. Buatlah sebuah **models** bernama `Diary` di dalam berkas `lab_3/models.py`:
    ```python
        from django.db import models

        # Create your models here.
        class Diary(models.Model):
            date = models.DateTimeField()
            activity = models.TextField(max_length=60)
    ```

15. Jalankan _Test Case_ kalian (`python manage.py test`), maka akan kembali muncul _Error_ pada _Test Case_

    > Loh Kenapa?
    >
    > Hal ini dikarenakan kalian belum melakukan _Database migrations_ dan proses _migrate_
    >
    > Jalankan perintah `python manage.py makemigrations` dan `python manage.py migrate` untuk menerapkan
    > perubahan yang sudah kamu lalukan pada semua berkas `models.py`     

    > `makemigrations` digunakan untuk mencatat dan membuat _migrations_ yang dilakukan pada _models_ (berkas `models.py`).
    > `migrate` digunakan untuk menerapkan _migrations_ yang ada.
    > Perhatikan bahwa perintah untuk menerapakan _migrations_ yang ada adalah `migrate` BUKAN `makemigrations`.

    > Penjelasan lebih lanjut mengenai _Migrations_ pada Django cek [link] (https://docs.djangoproject.com/en/1.11/topics/migrations/) berikut

16. Silahkan jalankan _test_ kalian lagi maka kalian bisa melihat semua _Test Case_ akan ter-_solved_. Tandanya kalian berhasil membuat database Diary.

    > `python manage.py test`

17. Buka kembali berkas `lab_3/tests.py` lalu tambahkan kode berikut pada baris paling akhir :
    ```python
        ........
        class Lab3Test(TestCase):
            ........
            def test_can_save_a_POST_request(self):
            response = self.client.post('/lab-3/add_activity/', data={'date': '2017-10-12T14:14', 'activity' : 'Maen Dota Kayaknya Enak'})
            counting_all_available_activity = Diary.objects.all().count()
            self.assertEqual(counting_all_available_activity, 1)

            self.assertEqual(response.status_code, 302)
            self.assertEqual(response['location'], '/lab-3/')

            new_response = self.client.get('/lab-3/')
            html_response = new_response.content.decode('utf8')
            self.assertIn('Maen Dota Kayaknya Enak', html_response)
    ```
18. Setelah itu jalankan kembali _Test Case_ ( `python manage.py test` ) , akan muncul _error_.

19. Buka berkas `lab_3/views.py` dan tambahkan baris kode berikut :
    ```python
    from django.shortcuts import redirect
    from .models import Diary
    from datetime import datetime
    import pytz
    ....
    def add_activity(request):
        if request.method == 'POST':
            date = datetime.strptime(request.POST['date'],'%Y-%m-%dT%H:%M')
            Diary.objects.create(date=date.replace(tzinfo=pytz.UTC),activity=request.POST['activity'])
            return redirect('/lab-3/')
    ```
20. Kemudian pada berkas `lab_3/urls.py` tambahkan kode berikut :
     ```python
    ...
    from .views import add_activity
    urlpatterns = [
        ...
        url(r'add_activity/$', add_activity, name='add_activity'),
    ]
     ```
21. Jalankan kembali _test_ untuk memastikan bahwa akan muncul _Error_
    >Baca baik-baik terminal kalian dimana letak errornya, lihat line code yang error, menurut kalian apa maksud dari line code yang error dan kenapa error bisa terjadi?

    >Hal ini dikarenakan data kalian sudah bisa dibuat di _Database_, namun data tersebut
    belum ditampilkan ke halaman depan. Untuk itu kita harus menampilkan kembali semua data yang sudah
    kita simpan di _Database_

22. Coba ulangi langkah 11, kali ini setelah kalian menekan tombol submit, maka error tidak akan terjadi namun aktivitas yang baru kalian tambahkan juga tidak muncul pada tabel

23. Untuk menampilkan data yang sudah tersimpan di _Database_ maka ubah kode yang ada
di `lab_3/views.py` sehingga menjadi seperti berikut:

    ```python
    from django.shortcuts import render, redirect
    from .models import Diary
    from datetime import datetime
    import pytz
    import json
    # Create your views here.
    diary_dict = {}
    def index(request):
        diary_dict = Diary.objects.all().values()
        return render(request, 'to_do_list.html', {'diary_dict' : convert_queryset_into_json(diary_dict)})

    def add_activity(request):
        if request.method == 'POST':
            date = datetime.strptime(request.POST['date'],'%Y-%m-%dT%H:%M')
            Diary.objects.create(date=date.replace(tzinfo=pytz.UTC),activity=request.POST['activity'])
            return redirect('/lab-3/')

    def convert_queryset_into_json(queryset):
        ret_val = []
        for data in queryset:
            ret_val.append(data)
        return ret_val
    ```

24. Coba jalankan _test_ kalian dan (seharusnya) _test_ kalian akan **passed**
    > ....................
    >
    > Ran 19 Test Ok

25. Coba jalankan halaman secara lokal, uji fitur diary kalian, sekarang kalian dapat melihat aktivitas yang kalian tambahkan muncul pada tabel aktivitas

26. Push code kalian agar dapat terdeploy ke heroku

## Challenge Checklist
[ ] Ubah warna tulisan `Hai semua, berikut adalah Catatan Harianku menjadi bukan warna hitam`
