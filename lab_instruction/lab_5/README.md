# Lab 5: Pengenalan _CSS_ dan _Responsive Web Design_

CSGE602022 - Web Design & Programming (Perancangan & Pemrograman Web) @
Faculty of Computer Science Universitas Indonesia, Odd Semester 2017/2018

* * *

## Tujuan Pembelajaran

Setelah menyelesaikan tutorial ini, mahasiswa diharapkan untuk mengerti 

- Mengerti cara penulisan CSS 
- Mengetahui static files dalam django
- Mengerti penggunaan selector pada CSS
- Mengerti mengenai konsep Responsive Web Design
- Mengetahui cara membuat website yang responsive

## Hasil Akhir Lab
- Membuat halaman TODO List dengan CSS yang sesuai

## Pengenalan CSS dan Responsive Web Design

#### CSS 

Cascading Style Sheets (CSS) adalah bahasa yang digunakan untuk mendeskripsikan tampilan dan format dari sebuah website yang ditulis pada markup language ( seperti HTML ). Kegunaannya menjadikan tampilan website lebih menarik.

#### Responsive Web Design

Responsive web design merupakan pendekatan untuk tampilan website tetap terlihat baik pada semua perangkat (baik desktop, tablets, atau phones). Responsive web design tidak mengubah content yang ada, hanya mengubah cara penyajian pada setiap perangkat agar sesuai. Responsive web design menggunakan CSS untuk resize, menyusutkan, atau membesarkan suatu element.

Silahkan buka link berikut untuk lebih lengkap mengenai `CSS` dan `Reponsive Web Design` dan mendukung pengerjaan Lab 5 anda.

CSS: (https://www.w3schools.com/css/default.asp)

Responsive Web Design: (https://developers.google.com/web/fundamentals/design-and-ux/responsive/patterns)

## Instruksi Lab 5: Membuat Halaman TODO List

2. Buatlah _apps_ baru bernama `lab_5` 
1. Masukkan `lab_5` kedalam `INSTALLED_APPS`
1. Buatlah _Test_ baru kedalam `lab_5/tests.py` : 
    ```python
    from django.test import TestCase
    from django.test import Client
    from django.urls import resolve
    from .views import index, add_todo
    from .models import Todo
    from .forms import Todo_Form
    
    # Create your tests here.
    class Lab5UnitTest(TestCase):
    
        def test_lab_5_url_is_exist(self):
            response = Client().get('/lab-5/')
            self.assertEqual(response.status_code, 200)
    
        def test_lab5_using_index_func(self):
            found = resolve('/lab-5/')
            self.assertEqual(found.func, index)
    
        def test_model_can_create_new_todo(self):
            # Creating a new activity
            new_activity = Todo.objects.create(title='mengerjakan lab ppw', description='mengerjakan lab_5 ppw')
    
            # Retrieving all available activity
            counting_all_available_todo = Todo.objects.all().count()
            self.assertEqual(counting_all_available_todo, 1)
    
        def test_form_todo_input_has_placeholder_and_css_classes(self):
            form = Todo_Form()
            self.assertIn('class="todo-form-input', form.as_p())
            self.assertIn('id="id_title"', form.as_p())
            self.assertIn('class="todo-form-textarea', form.as_p())
            self.assertIn('id="id_description', form.as_p())
    
        def test_form_validation_for_blank_items(self):
            form = Todo_Form(data={'title': '', 'description': ''})
            self.assertFalse(form.is_valid())
            self.assertEqual(
                form.errors['description'],
                ["This field is required."]
            )
        def test_lab5_post_success_and_render_the_result(self):
            test = 'Anonymous'
            response_post = Client().post('/lab-5/add_todo', {'title': test, 'description': test})
            self.assertEqual(response_post.status_code, 302)
    
            response= Client().get('/lab-5/')
            html_response = response.content.decode('utf8')
            self.assertIn(test, html_response)
    
        def test_lab5_post_error_and_render_the_result(self):
            test = 'Anonymous'
            response_post = Client().post('/lab-5/add_todo', {'title': '', 'description': ''})
            self.assertEqual(response_post.status_code, 302)
    
            response= Client().get('/lab-5/')
            html_response = response.content.decode('utf8')
            self.assertNotIn(test, html_response)
    ```
    
1. _Commit_ lalu _Push_ pekerjaan kalian, maka kalian akan melihat _UnitTest_ kalian akan _error_
1. Buatlah konfigurasi URL di `praktikum/urls.py` untuk `lab_5`
    ```python
    ........
    import lab_5.urls as lab_5
    
    urlpatterns = [
        .....
        url(r'^lab-5/', include(lab_5, namespace='lab-5')),
    ]
    ```
1. Buatlah konfigurasi URL di `lab_5/urls.py`
    ```python
    from django.conf.urls import url
    from .views import index, add_todo
    
    urlpatterns = [
        url(r'^$', index, name='index'),
        url(r'^add_todo', add_todo, name='add_todo'),
    ]
    ```
1. Buatlah _Models_ untuk `Todo` di dalam `lab_5/models.py`
    ```python
    from django.db import models

    class Todo(models.Model):
        title = models.CharField(max_length=27)
        description = models.TextField()
        created_date = models.DateTimeField(auto_now_add=True)
    ```
1. Jalankan perintah `makemigrations` dan `migrate`
1. Buat file `forms.py` dan masukan kode dibawah
    ```python
    from django import forms

    class Todo_Form(forms.Form):
        error_messages = {
            'required': 'Tolong isi input ini',
        }
        title_attrs = {
            'type': 'text',
            'class': 'todo-form-input',
            'placeholder':'Masukan judul...'
        }
        description_attrs = {
            'type': 'text',
            'cols': 50,
            'rows': 4,
            'class': 'todo-form-textarea',
            'placeholder':'Masukan deskripsi...'
        }

        title = forms.CharField(label='', required=True, max_length=27, widget=forms.TextInput(attrs=title_attrs))
        description = forms.CharField(label='', required=True, widget=forms.Textarea(attrs=description_attrs))
    ```
1. Buatlah fungsi `index` dan `add_todo` yang akan meng-_handle_ URL dari `lab_5`
    ```python
    from django.shortcuts import render
    from django.http import HttpResponseRedirect
    from .forms import Todo_Form
    from .models import Todo

    # Create your views here.
    response = {}
    def index(request):    
        response['author'] = "" #TODO Implement yourname
        todo = Todo.objects.all()
        response['todo'] = todo
        html = 'lab_5/lab_5.html'
        response['todo_form'] = Todo_Form
        return render(request, html, response)

    def add_todo(request):
        form = Todo_Form(request.POST or None)
        if(request.method == 'POST' and form.is_valid()):
            response['title'] = request.POST['title']
            response['description'] = request.POST['description']
            todo = Todo(title=response['title'],description=response['description'])
            todo.save()
            return HttpResponseRedirect('/lab-5/')
        else:
            return HttpResponseRedirect('/lab-5/')
    ```
1. Buatlah `lab_5.css` di dalam `lab_5/static/css` (Jika folder belum tersedia, silahkan membuat folder tersebut)
    ```css
    body{
      margin-top: 70px;
    }
    
    /* Custom navbar style */
    .navbar-static-top {
      margin-bottom: 19px;
    }
    .navbar-default .navbar-nav>li>a {
        cursor: pointer;
    }
    
    /* Textarea not resizeable */
    textarea {
        resize:none
    }
    section{
      min-height: 600px;
    }
    /* animasi pada title */
    .main-title{
      -webkit-animation: colorchange 1s infinite;
      -webkit-animation-direction: alternate;
      text-align: center;
    }
    
    /* animasi colorchange */
    @-webkit-keyframes colorchange {
        0% {
            -webkit-text-stroke: 5px #0fb8ad;
        letter-spacing: 0;
        }
      50% {
        -webkit-text-stroke: 7.5px  #1fc8db;
      }
        100% {
            -webkit-text-stroke: 10px  #2cb5e8;
        letter-spacing: 18px;
        }
    }
    /* styling wrapper form */
    #input-list{
      background: linear-gradient(to bottom right, #606062, #393939);
    }
    /* styling form */
    #input-list form{
      width: 400px;
      margin: 50px auto;
      text-align: center;
      position: relative;
      z-index: 1;
      background: white;
      border: 0 none;
      border-radius: 3px;
      box-shadow: 0 0 15px 1px rgba(0, 0, 0, 0.4);
      padding: 20px 30px;
      box-sizing: border-box;
      position: relative;
    }
    /* styling judul form, apakah arti dari tanda '>' ? */
    #input-list form > h2{
      font-size: 1.3em;
      text-transform: uppercase;
      color: #2C3E50;
      margin-bottom: 10px;
    }
    /* styling form input dan textarea, apakah arti dari tanda ',' ? */
    #input-list form > .todo-form-input, #input-list form > .todo-form-textarea{
      padding: 15px;
        border: 1px solid #ccc;
        border-radius: 3px;
        margin-bottom: 10px;
        width: 100%;
        box-sizing: border-box;
        color: #2C3E50;
        font-size: 13px;
    }
    /* styling footer */
    footer p{
      text-align: center;
      padding-top: 10px;
    }
    
    /* styling responsive max-width menandakan bahwa rule css ini hanya akan bekerja untuk layar dengan maksimal 768px, jika lebih dari 768px maka rule ini diabaikan */
    @media only screen and (max-width: 768px) {
      #input-list form {
          width: 290px;
      }
    }
    
    #input-list{
      background: linear-gradient(to bottom right, #606062, #393939);
    }
    #input-list form{
      width: 400px;
      margin: 50px auto;
      text-align: center;
      position: relative;
      z-index: 1;
      background: white;
      border: 0 none;
      border-radius: 3px;
      box-shadow: 0 0 15px 1px rgba(0, 0, 0, 0.4);
      padding: 20px 30px;
      box-sizing: border-box;
      position: relative;
    }
    #input-list form > h2{
      font-size: 1.3em;
      text-transform: uppercase;
      color: #2C3E50;
      margin-bottom: 10px;
    }
    #input-list form > .todo-form-input, #input-list form > .todo-form-textarea{
      padding: 15px;
        border: 1px solid #ccc;
        border-radius: 3px;
        margin-bottom: 10px;
        width: 100%;
        box-sizing: border-box;
        color: #2C3E50;
        font-size: 13px;
    }
    
    #my-list{
      background: linear-gradient(141deg, #0fb8ad 0%, #1fc8db 51%, #2cb5e8 75%);
    }
    #my-list .my-list-title{
      font-size: 40px;
      margin-bottom: 1em;
      color: white;
      text-shadow: 3px 3px 0 #000, -1px -1px 0 #000, 1px -1px 0 #000, -1px 1px 0 #000, 1px 1px 0 #000;
      text-align: center;
    }
    
    .to-do-list{
      text-align: center;
      background: #fff;
      position: relative;
      z-index: 15;
      margin-bottom: 60px;
      -webkit-box-shadow: 0 5px 10px 0 rgba(0, 0, 0, 0.15);
      -moz-box-shadow: 0 5px 10px 0 rgba(0, 0, 0, 0.15);
      -o-box-shadow: 0 5px 10px 0 rgba(0, 0, 0, 0.15);
      box-shadow: 0 5px 10px 0 rgba(0, 0, 0, 0.15);
      transition: 0.5s all;
      -webkit-transition: 0.5s all;
      -moz-transition: 0.5s all;
      -o-transition: 0.5s all;
    }
    .to-do-list:after{
        content:"";
        display:block;
        height:25px; /* Which is the padding of div.container */
        background: #fff;
    }
    .flex{
      display: flex;
      flex-direction: row;
      flex-wrap: wrap;
      justify-content: flex-start;
      align-items: flex-start;
      align-content: flex-start;
      margin-right: -15px;
      margin-left: -15px;
    }
    .flex-item{
      padding: 0 15px;
      flex-grow: 0;
      flex-shrink: 0;
      flex-basis: 25%;
    }
    
    .to-do-list .to-do-list-title {
      font-size: 20px;
      font-weight: 700;
      padding: 10px 10px 0;
    }
    .to-do-list .to-do-list-date-added {
      font-size: 12px;
      padding: 0 10px;
    }
    .to-do-list .to-do-list-description {
      padding: 10px 25px;
      z-index: 15;
      background: #fff;
      height: 125px;
      text-align: justify;
      overflow: auto;
    }
    .to-do-list .to-do-list-delete {
      background: #e45;
      color: #fff;
      padding: 10px;
      font-size: 16px;
      width: 100%;
      height: 50px;
      bottom: 1px;
      position: absolute;
      cursor: pointer;
      z-index: -1;
      -webkit-border-radius: 0 0px 2px 2px;
      -moz-border-radius: 0 0px 2px 2px;
      -o-border-radius: 0 0px 2px 2px;
      border-radius: 0 0px 2px 2px;
      -webkit-transition: all 0.3s ease;
      -moz-transition: all 0.3s ease;
      -o-transition: all 0.3s ease;
      transition: all 0.3s ease;
    }
    .to-do-list .to-do-list-delete {
      bottom: -50px;
    }

    ```
1. Buatlah `base.html` di dalam `lab_5/templates/lab_5/layout` (Jika folder belum tersedia, silahkan membuat folder tersebut)
    ```html
    {% load staticfiles %}
    {% load static %}
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="LAB 5">
        <meta name="author" content="{{author}}">
        <!-- bootstrap csss -->
        

        <title>
            {% block title %} Lab 5 By {{author}} {% endblock %}
        </title>
    </head>

    <body>
        <header>
            {% include "lab_5/partials/header.html" %}
        </header>
        <content>
                {% block content %}
                    <!-- content goes here -->
                {% endblock %}
        </content>
        <footer>
            <!-- TODO Block Footer dan include footer.html -->
            {% block footer %}
            {% include "lab_5/partials/footer.html" %}
            {% endblock %}
        </footer>

        <!-- Jquery n Bootstrap Script -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script type="application/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script type="application/javascript">
          $(function() {
              $('a[href*="#"]:not([href="#"])').click(function() {
                if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
                  var target = $(this.hash);
                  target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
                  if (target.length) {
                    $('html, body').animate({
                      scrollTop: target.offset().top - 55
                    }, 1000);
                    return false;
                  }
                }
              });
            });
        </script>
    </body>
    </html>
    ```
1. Agar bisa mengakses _bootstrap_ serta _css_ yang sudah kalian buat, tambahkan link ke `lab_5.css` pada `base.html`
    ```html
    <head>
    ...
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{% static 'css/lab_5.css' %}" />
    ...
    </head>
    ```
1.  Buatlah `header.html` dan `footer.html` pada folder `templates/lab_5/partials`

    ```html
    <!-- templates/lab_5/partials/header.html -->
    <nav>
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="">Home</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <!-- LINK MENUJU table, dan link menuju home#form -->
            <li><a href="{% url 'lab-5:index' %}#input-list">Input Todo</a></li>
            <li><a href="{% url 'lab-5:index' %}#my-list">My List</a> </li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>
    ...
    ```
    ```html
    <!-- templates/lab_5/partials/footer.html -->
    <!-- TODO, CREATE THIS FILE AND ADD copyright symbol -->
    <p>Made with Love by {{author}}</p>

    ```
1. Buatlah `lab_5.html` di dalam `lab_5/templates/lab_5/`
    ```html
    {% extends "lab_5/layout/base.html" %}

    {% block content %}
    <h1 class="main-title">TODOLIST</h1>
    <hr/>
    <section name="input-list" id="input-list">
        <div class="container">
            <form id="form" method="POST" action="{% url 'lab-5:add_todo' %}">
                <h2>Input Todo</h2>
                {% csrf_token %}
                {{ todo_form }}
                <input id="submit" type="submit" class="btn btn-lg btn-block btn-info" value="Submit">
                <br>
            </form>
        </div>
    </section>
    <section name="my-list" id="my-list">
        <div class="container">
            <h2 class="my-list-title">My List</h2>
            <div class="flex">
                {% if todo %}
                    {% for data in todo %}
                        <div class="flex-item">
                            <div class="to-do-list">
                                <div class="to-do-list-title">
                                    {{data.title}}
                                </div>
                                <div class="to-do-list-date-added">
                                    {{data.created_date}}
                                </div>
                                <div class="to-do-list-description">
                                    {{data.description}}
                                </div>
                                <div class="to-do-list-delete">
                                    <div class="to-do-list-delete-button" data-id="{{data.id}}">Delete</div>
                                </div>
                            </div>
                        </div>
                    {% endfor %}
                {% else %}
                <div class="alert alert-danger text-center">
                    <strong>Oops!</strong> Tidak ada data Todo.
                </div>
                {% endif %}
            </div>
        </div>
    </section>


    {% endblock %}        
    ```
1. Aturlah `static file` agar dapat di deploy pada heroku. 
    1. Install whitenoise dengan perintah berikut.
        > pipenv install whitenoise
    2. Pada `settings.py` pastikan pengaturan middleware dan storagenya seperti berikut ini.
        ```python
        MIDDLEWARE = (
            # Simplified static file serving.
            # https://warehouse.python.org/project/whitenoise/
            'whitenoise.middleware.WhiteNoiseMiddleware',
            ...
        ```
        ```python
            ...
            # Simplified static file serving.
            # https://warehouse.python.org/project/whitenoise/

            STATICFILES_STORAGE = 'whitenoise.storage.CompressedManifestStaticFilesStorage'
        ```
    3. Pada proses build, heroku akan secara otomatis menjalankan ` python manage.py collectstatic --noinput`. Jika terjadi kegagalan pada proses build maka jalankan `heroku config:set DEBUG_COLLECTSTATIC=1` untuk mengetahui kesalahan yang terjadi.
    4. Jika masih kesulitan dalam proses deploy, dapat mengunjungi link ini. [Help Deploy Heroku](https://devcenter.heroku.com/articles/django-assets)

1. _Commit_ dan _push_. Kemudian, lanjutkan pada bagian testing.

## Melakukan Testing menggunakan Selenium

#### Testing dengan selenium pada local environment

1.  Buat `class` baru `tests.py` sebagai berikut.

    ```python
    .............
    from selenium import webdriver
    from selenium.webdriver.common.keys import Keys
    from selenium.webdriver.chrome.options import Options
    
    
    
    # Create your tests here.
    class Lab5FunctionalTest(TestCase):
    
        def setUp(self):
            chrome_options = Options()
            self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
            super(Lab5FunctionalTest, self).setUp()
    
        def tearDown(self):
            self.selenium.quit()
            super(Lab5FunctionalTest, self).tearDown()
    
        def test_input_todo(self):
            selenium = self.selenium
            # Opening the link we want to test
            selenium.get('http://127.0.0.1:8000/lab-5/')
            # find the form element
            title = selenium.find_element_by_id('id_title')
            description = selenium.find_element_by_id('id_description')
    
            submit = selenium.find_element_by_id('submit')
    
            # Fill the form with data
            title.send_keys('Mengerjakan Lab PPW')
            description.send_keys('Lab kali ini membahas tentang CSS dengan penggunaan Selenium untuk Test nya')
    
            # submitting the form
            submit.send_keys(Keys.RETURN)
    ```
2.  Dapat dilihat pada `tests.py` terdapat import library `selenium`
    ```python
    from selenium import webdriver
    from selenium.webdriver.common.keys import Keys
    from selenium.webdriver.chrome.options import Options
    ...
    ```
2. Install `selenium` dengan command:
    > pip install selenium

3. Download `chromedriver` sesuai dengan OS kalian pada url berikut.
[Download chromedriver](https://chromedriver.storage.googleapis.com/index.html?path=2.32/)

4. `unzip` chromedriver pada folder project kalian. (Akan menghasilkan `chromedriver.exe` pada Windows dan `chromedriver` pada Linux dan Mac). Berikut struktur folder (contoh pada windows)

            lab-ppw
    		├──lab_1
    		├──lab_2
    		├──...
            ├──lab_5
            ├── manage.py
            ├──...
            ├── README.md
            ├── chromedriver.exe
            ├──...

5. Mengatur `tests.py` seperti berikut. (contoh pada windows)
    ```python
    ...
    def setUp(self):
        chrome_options = Options()
        self.selenium  = webdriver.Chrome('./chromedriver.exe', chrome_options=chrome_options)
        super(Lab5FunctionalTest, self).setUp()
    ...
    ```

6. Pastikan kalian memiliki chrome browser.
7. Jalankan `./manage.py collectstatic`
7. Jalankan `./manage.py runserver`
8. Buka terminal baru, jalankan test dengan `./manage.py test`
9. Lihat chrome browser akan terbuka dan melakukan test secara otomatis.

#### Testing dengan selenium dengan gitlab-ci

1. Pastikan kalian telah melakukan **Testing dengan selenium pada local environment** langkah 1 hingga 4.

2. Mengatur `tests.py` seperti berikut.
    ```python
    ...
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(Lab5FunctionalTest, self).setUp()
    ...
    ```

3. Pastikan file `.gitignore` pada baris terakhir memiliki ini.

    ```bash
    ...
    #ignore chromedriver
    chromedriver
    chromedriver.exe
    /pratikum/static/*
    ```
4. _Commit_ dan _push_.

Penjelasan mengenai selenium :
[Click me](http://www.seleniumhq.org/)